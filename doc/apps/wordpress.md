# Deploy a wordpress site

Create a VPS as described here : [Create a VPS](../Create a VPS.md)
and setup a Dockerfile in `images/phpserver` for the wordpress server
with the example in the library : [wordpress](../../library/images/wordpress)

The host server should install th wp-cron script as described in [the install procedure](../Install.md)

## Docker compose example

```yaml
version: '2'

networks:
  proxy:
    external: true

services:
  web:
    build: ./images/phpserver
    ## it's important to use .wordpress to detect container running wordpress and used by the host wp-cron script
    container_name: ${WEBVPS_NAME}.wordpress
    user: ${WEBVPS_WORKER_UID}:${WEBVPS_WORKER_UID}
    restart: unless-stopped
    cpu_shares: 128
    mem_limit: 1g
    memswap_limit: 1g
    environment:
      VIRTUAL_HOST: ${WEBVPS_DOMAINS},${WEBVPS_NAME}.your-domain-host-server.com
      APACHE_RUN_USER: "#${WEBVPS_WORKER_UID}"
      WEBVPS_WORKER_UID: ${WEBVPS_WORKER_UID}
      DB_NAME: ${WEBVPS_NAME}
      DB_HOST: host:port
      DB_USER: ${WEBVPS_NAME}
      DB_PASSWORD: ${WEBVPS_DB_PASSWORD}
      WP_SITE_URL: https://www.${WEBVPS_HOST}
      WP_SITE_TITLE: ${WEBVPS_NAME}
      WP_ADMIN_USERNAME: ${WEBVPS_NAME}
      WP_ADMIN_EMAIL: admin@mail.com
      LETSENCRYPT_HOST: ${WEBVPS_DOMAINS},${WEBVPS_NAME}.your-domain-host-server.com
      LETSENCRYPT_EMAIL: youradmin@email.com
    entrypoint:
      - bash
      - -c
      - |        
        wp-install
        exec apache2-foreground
    volumes:
      - ${WEBVPS_VOLUME_ROOT}/html:/var/www/html/wordpress
    networks:
      - proxy
      - default
```

## Configure Easy WP smtp

On starting, the container install wordpress with the easy SMTP plugin because it
can't send email. It's a choice to avoid a black list of the server.

So you have to configure Easy SMTP and Wordpress could sent emails.

## Migration from another hosting solution

First you can rsync data : [Rsync data to VPS](../rsync-data-to-vps.md)

After it, restore the database in the new database server and upgrade the database
with the plugin https://fr.wordpress.org/plugins/better-search-replace/

- To change the original install path
- To change the domain if required

You can also check :

- the `.htaccess` file to change older paths or domains
- the `wp-config.php` without forgotting to set `define( 'DISABLE_WP_CRON', true );`
