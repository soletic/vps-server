# Manage nextcloud instance

## Docker compose

NextCloud image built from source [library/images/nextcloud](../../library/images/nextcloud)

```yaml
# docker-compose.yml
version: '2'

networks:
  proxy:
    external: true

services:
  db:
    build: ./images/mariadb
    container_name: ${WEBVPS_NAME}.mysql
    restart: unless-stopped
    cpu_shares: 128
    mem_limit: 1g
    memswap_limit: 1g
    volumes:
      - ${WEBVPS_VOLUME_ROOT}/db:/var/lib/mysql
      - ${WEBVPS_VOLUME_ROOT}/backup:/backup
    environment:
      - WORKER_UID=${WEBVPS_WORKER_UID}
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=${WEBVPS_NAME}
      - MYSQL_PASSWORD=${WEBVPS_DB_PASSWORD}
      - MYSQL_ROOT_PASSWORD=${WEBVPS_DB_ROOT_PASSWORD}
    networks:
      - default
  redis:
      image: redis:alpine
      container_name: ${WEBVPS_NAME}.redis
      restart: unless-stopped
      mem_limit: 512m
      memswap_limit: 512m
      command: redis-server --save 20 1 --loglevel warning --requirepass ${WEBVPS_DB_PASSWORD}
      volumes:
          - ${WEBVPS_VOLUME_ROOT}/redis:/data
      networks:
          - default
  web:
    build: ./images/nextcloud
    container_name: ${WEBVPS_NAME}.nextcloud
    cpu_shares: 128
    mem_limit: 1g
    memswap_limit: 1g
    environment:
      APACHE_RUN_USER: "#${WEBVPS_WORKER_UID}"
      APACHE_RUN_GROUP: "#${WEBVPS_WORKER_UID}"
      WORKER_UID: ${WEBVPS_WORKER_UID}
      VIRTUAL_HOST: ${WEBVPS_DOMAINS}
      NEXTCLOUD_ADMIN_USER: ${WEBVPS_NAME}
      NEXTCLOUD_ADMIN_PASSWORD: ${WEBVPS_NC_ADMIN_PASSWORD}
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: root
      MYSQL_PASSWORD: ${WEBVPS_DB_PASSWORD}
      MYSQL_HOST: db
      MAIL_FROM_ADDRESS: no-reply@yourdomain.com
      SMTP_HOST: smtp.yourserver.com
      SMTP_PORT: 465
      SMTP_SECURE: ssl
      SMTP_NAME: name
      SMTP_PASSWORD: password
      MAIL_DOMAIN: soletic.org
      PHP_MEMORY_LIMIT: 512m
      PHP_UPLOAD_LIMIT: 512m
      LETSENCRYPT_HOST: ${WEBVPS_DOMAINS}
      LETSENCRYPT_EMAIL: admin@yourdomain.com
    links:
      - db
    volumes:
      - ${WEBVPS_VOLUME_ROOT}/nextcloud:/var/www/html
      - ${WEBVPS_VOLUME_ROOT}/custom_apps:/var/www/html/custom_apps
      - ${WEBVPS_VOLUME_ROOT}/config:/var/www/html/config
      - ${WEBVPS_VOLUME_ROOT}/data:/var/www/html/data
    restart: unless-stopped
    networks:
      - default
      - proxy

  phpmyadmin:
    container_name: ${WEBVPS_NAME}.phpmyadmin
    hostname: phpmyadmin.${WEBVPS_HOST}
    image: phpmyadmin/phpmyadmin
    restart: unless-stopped
    mem_limit: 512m
    memswap_limit: 512m
    links:
      - db
    environment:
      VIRTUAL_HOST: db.${WEBVPS_HOST}
      LETSENCRYPT_HOST: db.${WEBVPS_HOST}
      LETSENCRYPT_EMAIL: admin@yourdomain.com
    networks:
      - proxy
      - default
```

## Cron

Add a cronjob to run regularly php -f cron.php

**Example**, in the host if the container name is `soletic_nc.nextcloud`

```bash
sudo -i
crontab -e
```

And add line

```cron
*/5  *  *  *  * docker exec -it -u www-data soletic_nc.nextcloud bash -c 'php -f cron.php'
```

Or create the bash file `/etc/cron.hourly/nextcloud-cron` :

```bash
#!/bin/bash

docker exec -u www-data soletic_nc.nextcloud bash -c 'php -f cron.php'
```

## Upgrade

Avant la mise à jour, il faut s'assurer que les applications tierces installées sont compatibles. Voici généralement les applications courantes à controler

- [Deck](https://apps.nextcloud.com/apps/deck)
- [Keeweb](https://apps.nextcloud.com/apps/keeweb)
- [Group Folders](https://apps.nextcloud.com/apps/groupfolders)
- [OnlyOffice](https://apps.nextcloud.com/apps/onlyoffice)
- [Appointments](https://apps.nextcloud.com/apps/appointments)
- [QownNotes](https://apps.nextcloud.com/apps/qownnotesapi)
- [Talk / Spreed](https://apps.nextcloud.com/apps/spreed)
- [Tasks](https://apps.nextcloud.com/apps/tasks)
- [Nextcloud Office / Collabora](https://apps.nextcloud.com/apps/richdocuments)

Désactiver les jobs crontab de nextcloud installés sur l'hôte ou stopper le service
cron le temps de la mise à jour :

- `sudo /etc/init.d/cron stop`
- ou `vi /etc/cron.hourly/nextcloud-cron` et `vi /etc/cron.hourly/vps-backup`

Si vous faites une mise à jour de changement majeur de version (par exemple passage de netcloud 23 à 24),
penser à bien relire le Readme de l'image docker notamment pour vérifier :

- S'il n'y a pas des instructions spécifiques de mises à jour
- S'il des données persistantes sont à ajouter au docker-compose

```
cd /path/to/vps
. .env
docker pull nextcloud:23-apache
vps-compose build
docker exec -it -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ maintenance:mode --on'
docker stop ${WEBVPS_NAME}.nextcloud && docker rm ${WEBVPS_NAME}.nextcloud && vps-compose up
```

Suivre le lancement de la mise à jour

```
docker logs --tail 100 -f ${WEBVPS_NAME}.nextcloud
```

Lancer une mise à jour la main ensuite :

```
docker exec -it -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ upgrade'
```

Des extensions peuvent se retrouver désactivées, car incompatible. Normalement, elles sont mises à jour à la fin et réactiver. Il faut donc le contrôler.
Upgrade take a long time (5 minutes, depends on size of the instance).

```
docker exec -it -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ maintenance:mode --off'
```

After starting the new version, read recommandations in the page parameters : https://yourdomain.com/settings/admin/overview

Puis, exécuter les tâches de fond

```
docker exec -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php -f cron.php'
```

Vérifier si des recommandations sont faites sur l'instance : https://host.nextcloud/settings/admin/overview.
Généralement il y a des index à créer :

```
docker exec -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ db:add-missing-columns'
docker exec -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ db:add-missing-indices'
docker exec -u www-data ${WEBVPS_NAME}.nextcloud bash -c 'php occ db:add-missing-primary-keys'
```

Ré-activer crontab.
