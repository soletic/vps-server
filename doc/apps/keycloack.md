# Manage Keycloack instance

Il y a deux façons documentées sur internet qui explique comment installer Keycloack :

1. Une basée sur la documentation de l'image jboss/keycloack qui au 22 novembre package keycloack en version 16
2. Une basée sur la documentation de Keycloack qui va proposer la dernière version qui depuis la 17 est basé sur quarkus pour configuration - [Migrating to Quarkus distribution](https://www.keycloak.org/migration/migrating-to-quarkus)

Documentations sources :

- [Running Keycloak in a container](https://www.keycloak.org/server/containers) - La documentation de Keycloack
- [Keycloak Docker setup with NGINX Proxy and Letsencrypt](https://github.com/selloween/docker-keycloak-letsencrypt) - Une docker compose compose complet avec jboss/keycloack
- [Run Keycloak Server in Docker Containers with Let’s Encrypt SSL](https://computingforgeeks.com/run-keycloak-server-in-docker/) - 28 juin 2022 - à regarder en priorité car propose les deux versions d'install.
- [nginx-proxy / nginx-proxy](https://github.com/nginx-proxy/nginx-proxy) to find how to configure proxy

## VPS and database

[Create the](../Create a VPS.md) VPS or use an existing one.

Create the postgres data folder : `sudo mkdir /path/to/vps-data/postgresql`

Create the appropriate `.env` file to configure environment parameter used in the following docker-compose file.

```
#!/bin/bash
export WEBVPS_NAME=keycloack_instance
export WEBVPS_VOLUME_ROOT=

export KC_HOST=auth.example.org
export KC_POSTGRES_USER=user
export KC_POSTGRES_PASSWORD=Passw0rd
export KC_POSTGRES_DB=keycloakdb
export KC_USER_ADMIN=admin
export KC_ADMIN_PASSWORD=Passw0rd
export KC_DB_URL=jdbc:postgresql://keycloack_db/keycloakdb
```

## Setup with jboss/keycloack

> Non encore testé

Image : https://hub.docker.com/r/jboss/keycloak/tags

```
# docker-compose.yml
version: '2'

networks:
  proxy:
    external: true

volumes:
  postgres_keycloack_data:
    driver_opts:
      type: none
      device: /path/to/vps-data/postgresql
      o: bind    

services:
  keycloack:
    image: jboss/keycloack
    container_name: ${WEBVPS_NAME}.keycloack
    cpu_shares: 128
    mem_limit: 512m
    memswap_limit: 512m
    environment:

      KEYCLOAK_DB_USERNAME: ${KC_POSTGRES_USER}
      KEYCLOAK_DB_PASSWORD: ${KC_POSTGRES_PASSWORD}
      JDBC_PARAMS: ssl=false
      KEYCLOAK_HOSTNAME: ${KC_HOST}
      KEYCLOAK_HTTP_PORT: 8080
      KEYCLOAK_ADMIN: ${KC_USER_ADMIN}
      KEYCLOAK_ADMIN_PASSWORD: ${KC_ADMIN_PASSWORD}
      VIRTUAL_HOST: ${KC_HOST}
      VIRTUAL_PORT: 8080
      PROXY_ADDRESS_FORWARDING: "true"
      LETSENCRYPT_HOST: ${KC_HOST}
      LETSENCRYPT_EMAIL: admin@yourdomain.com
    depends_on:
      - keycloack_db
    entrypoint:
      - opt/keycloak/bin/kc.sh
      - "start --db=postgres"
    restart: unless-stopped
    networks:
      - default
      - proxy

  keycloack_db:
    image: postgres:15
    container_name: ${WEBVPS_NAME}.postgres
    restart: unless-stopped
    cpu_shares: 128
    mem_limit: 512m
    memswap_limit: 512m
    environment:
      POSTGRES_DB: ${KC_POSTGRES_DB}
      POSTGRES_USER: ${KC_POSTGRES_USER}
      POSTGRES_PASSWORD: ${KC_POSTGRES_PASSWORD}
    volumes:
      - postgres_keycloack_data:/var/lib/postgresql/data
    networks:
      - default
```

## Setup by building image

> Work in progress. Does not work at the moment. We have to fix
> - Datasource <default> enables XA but transaction recovery is not enabled. Please enable transaction recovery by setting quarkus.transaction-manager.enable-recovery=true, otherwise data may be lost if the application is terminated abruptly
> - Does the image use the KEYCLOAK_HOSTNAME and PORT environments variables because the log displays : Hostname settings: Base URL: <unset>, Hostname: localhost, Strict HTTPS: true, Path: <request>, Strict BackChannel: false, Admin URL: <unset>, Admin: <request>, Port: -1, Proxied: false
>
> Sans doute du au fait que les docs lues étaient basées sur jboss
> PROXY_ADDRESS_FORWARDING ne devrait pas fonctionner non plus. Sans doute une configuration différente à faire.

Create a docker file image in `images/keycloack` by following the [Running Keycloak in a container](https://www.keycloak.org/server/containers) documentation.
Example on 11 november 2022 :

```
FROM quay.io/keycloak/keycloak:latest as builder

# Enable health and metrics support
ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true

#ENV KC_FEATURES=token-exchange

# Configure a database vendor
ENV KC_DB=postgres

WORKDIR /opt/keycloak
# for demonstration purposes only, please make sure to use proper certificates in production instead
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:latest
COPY --from=builder /opt/keycloak/ /opt/keycloak/

# Postgres connexion
ENV KC_DB_URL=jdbc:postgresql://db/keycloakdb
ENV KC_DB_USERNAME=keycloack
ENV KC_DB_PASSWORD=Passw0rd
ENV KC_HOSTNAME=localhost

ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
```

The docker-compose file :

```
# docker-compose.yml
version: '2'

networks:
  proxy:
    external: true

volumes:
  postgres_keycloack_data:
    driver_opts:
      type: none
      device: /path/to/vps-data/postgresql
      o: bind    

services:
  keycloack:
    build: ./images/keycloack
    container_name: ${WEBVPS_NAME}.keycloack
    cpu_shares: 128
    mem_limit: 512m
    memswap_limit: 512m
    environment:
      KC_DB_URL: "jdbc:postgresql://keycloack_db/${KC_POSTGRES_DB}"
      KC_DB_USERNAME: ${KC_POSTGRES_USER}
      KC_DB_PASSWORD: ${KC_POSTGRES_PASSWORD}
      JDBC_PARAMS: ssl=false
      KEYCLOAK_HOSTNAME: ${KC_HOST}
      KEYCLOAK_HTTP_PORT: 8080
      KEYCLOAK_ADMIN: ${KC_USER_ADMIN}
      KEYCLOAK_ADMIN_PASSWORD: ${KC_ADMIN_PASSWORD}
      VIRTUAL_HOST: ${KC_HOST}
      VIRTUAL_PORT: 8080
      PROXY_ADDRESS_FORWARDING: "true"
      LETSENCRYPT_HOST: ${KC_HOST}
      LETSENCRYPT_EMAIL: admin@yourdomain.com
    depends_on:
      - keycloack_db
    entrypoint:
      - opt/keycloak/bin/kc.sh
      - "start --db=postgres"
    restart: unless-stopped
    networks:
      - default
      - proxy

  keycloack_db:
    image: postgres:15
    container_name: ${WEBVPS_NAME}.postgres
    restart: unless-stopped
    cpu_shares: 128
    mem_limit: 512m
    memswap_limit: 512m
    environment:
      POSTGRES_DB: ${KC_POSTGRES_DB}
      POSTGRES_USER: ${KC_POSTGRES_USER}
      POSTGRES_PASSWORD: ${KC_POSTGRES_PASSWORD}
    volumes:
      - postgres_keycloack_data:/var/lib/postgresql/data
    networks:
      - default
```
