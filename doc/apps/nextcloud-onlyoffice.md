# Nextcloud avec Onlyoffice

Tout d'abord lire [nextcloud.md](nextcloud.md) qui est la base pour déployer
nextcloud au sein d'un VPS.

## Install

Ajouter à votre docker-compose.yml le micro-service

```yaml
# docker-compose.yml

# ...
services:
  onlyoffice:
    container_name: ${WEBVPS_NAME}.onlyoffice
    image: onlyoffice/documentserver:latest
    stdin_open: true
    tty: true
    restart: unless-stopped
    cpu_shares: 256
    mem_limit: 2g
    memswap_limit: 2g
    environment:
      VIRTUAL_HOST: onlyoffice.${WEBVPS_HOST}
      LETSENCRYPT_HOST: onlyoffice.${WEBVPS_HOST}
      LETSENCRYPT_EMAIL: admin@yourdomain.com
    volumes:
      - ${WEBVPS_VOLUME_ROOT}/onlyoffice/log:/var/log/onlyoffice
      - ${WEBVPS_VOLUME_ROOT}/onlyoffice/data:/var/www/onlyoffice/Data
    networks:
      - default
      - proxy
# ...
```

## Upgrade

Avant la mise à jour, il faut s'assurer que les applications tierces installées sont compatibles. Voici généralement les applications courantes à controler

- [OnlyOffice](https://apps.nextcloud.com/apps/onlyoffice)

Désactiver les jobs crontab de nextcloud installés sur l'hôte ou stopper le service
cron le temps de la mise à jour :

- `sudo /etc/init.d/cron stop`
- ou `vi /etc/cron.hourly/nextcloud-cron`

Si vous faites une mise à jour de changement majeur de version,
penser à bien relire le Readme de l'image docker notamment pour vérifier :

- S'il n'y a pas des instructions spécifiques de mises à jour
- S'il des données persistantes sont à ajouter au docker-compose

```
cd /path/to/vps
. .env
docker pull onlyoffice/documentserver:latest
vps-compose build
docker stop ${WEBVPS_NAME}.onlyoffice
docker rm ${WEBVPS_NAME}.onlyoffice
vps-compose up
```

Suivre le lancement de la mise à jour

```
docker logs --tail 100 -f ${WEBVPS_NAME}.onlyoffice
```
