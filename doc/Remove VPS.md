# Remove a VPS

## Remove the user reference

```bash
export WEBVPS_NAME=vps-example
export WEBVPS_WORKER_UID=10031
sudo userdel ${WEBVPS_NAME}
sudo groupdel ${WEBVPS_NAME}
```

## Remove FTP entry

```bash
sudo vi /mnt/data/vps/ftpd/passwd/pureftpd.passwd 
```