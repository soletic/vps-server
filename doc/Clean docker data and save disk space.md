# Clean docker data and save disk space

Bibliographie : 

- https://medium.com/better-programming/docker-tips-clean-up-your-local-machine-35f370a01a78 
- https://docs.docker.com/config/pruning/

## Repérer ce qui prendre la place

```bash
docker system df
```

Permet de savoir ce qui prend de la place : les images ? les containers ? les volumes ? les networks ?

## Nettoyer le système complet

`docker system prune`

To remove all images which are not used by existing containers, use the -a flag

`docker image prune -a`

*Et ci-dessous, c'est si l'on veut être plus fin*

**delete all stopped containers at once and to reclaim the disk space they’re using**

`docker container prune`

## Recréer les containers volumineux

Si ce sont les containers alors il faut recréer ceux qui sont volumineux.
Pour les répérer, on peut utiliser la commande suivante :

```bash
docker ps --size
```

