# Resize data disk

[Documentation inspirée par le guide d'OVH](https://docs.ovh.com/fr/public-cloud/augmenter-la-taille-dun-disque-supplementaire/) puis de [Live resizing of an ext4 filesytem on linux](https://codesilence.wordpress.com/2013/03/14/live-resizing-of-an-ext4-filesytem-on-linux/). Cette opération est à faire aussi bien sur le disque de données que celui qui contient la réplication.

**Couper docker** ```sudo service docker stop```

**Sauvegarder le disque** : ```sudo /etc/cron.daily/vps-replication```

**Démonter le disque** puis redimensionner le disque dans l'interface d'OVH.

**Déterminer quel est le disque à redimensionner**

Exécuter la commande ```lsblk```. Vous obtiendrez par exemple ```/dev/sdb1```

Attention, si vous aviez plusieurs disques et que vous redimensionnez un disque qui n'est pas le dernier de la liste alors il vous se retrouver en dernier et tous les disques vous avoir leur lettre de changer (sdc devient sdb et sdb devient sdc). Il convient donc de démonter puis détacher tous les disques pour les rattacher dans le bon ordre.

**Agrandir le disque** 

Exécuter la commande ```sudo fdisk /dev/sdb``` (sans le chiffre)

Tapez les commandes successives ```p`` (display partitions) ```d``` (delete) ```n``` (new) ```p``` (primary) ```1``` ```First sector : default``` ```Last sector : default``` ```N``` (to keep ext4 signature) ```w``` (enregistrer)

```
sudo e2fsck -f /dev/sdb1
sudo resize2fs /dev/sdb1
sudo service docker start
```

