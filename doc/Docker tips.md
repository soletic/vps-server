# Docker tips

Commandes pour vérifier consommation :

```bash
docker ps -q | xargs docker stats --no-stream | grep ssn
```

Regarder les erreurs de connection au container par le proxy :

```bash
docker logs http-proxy | grep 'server: www.salon'
```
