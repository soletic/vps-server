# Create a VPS

Se connecter au serveur en ssh en tant que ```ubuntu``` ou l'utilisateur autre que root et choisi pour administrer les déploiements de VPS

**Créer le dossier du VPS** dans ```/vps/{vps_name}``` :

```
export WEBVPS_NAME=vps-example
export WEBVPS_WORKER_UID=10031
mkdir -p /vps/${WEBVPS_NAME}
mkdir -p /mnt/data/vps/${WEBVPS_NAME}
```

Le nom du service doit être compatible pour une utilisation dans un nom de domaine. Utilisez donc uniquements une chaine respectant l'expression régulière suivante : ```^[a-z\-]+$```

**Créer l'utilisateur système référence** sous lequel tournera les services au sein des containers composant le VPS.

```
USER_PASSWORD=$(pwgen -s 12 1)
sudo groupadd -g ${WEBVPS_WORKER_UID} ${WEBVPS_NAME}
sudo useradd ${WEBVPS_NAME} --uid ${WEBVPS_WORKER_UID} --home /mnt/data/vps/${WEBVPS_NAME} --gid ${WEBVPS_WORKER_UID} --groups ${WEBVPS_NAME} --shell /bin/bash
sudo bash -c "echo \"${WEBVPS_NAME}:${USER_PASSWORD}\" | chpasswd"
sudo chown -R ${WEBVPS_NAME}:${WEBVPS_NAME} /mnt/data/vps/${WEBVPS_NAME}
```

Puis **créer le fichier ```.env```*** contenant les variables d'environnement permettant de faire fonctionner le VPS. Example :

```
WEBVPS_DB_ROOT_PASSWORD=$(pwgen -s 12 1)
WEBVPS_DB_PASSWORD=$(pwgen -s 12 1)
echo '#!/bin/bash' > /vps/${WEBVPS_NAME}/.env
echo "export WEBVPS_NAME=${WEBVPS_NAME}" >> /vps/${WEBVPS_NAME}/.env
echo 'export WEBVPS_VOLUME_ROOT=/mnt/data/vps/${WEBVPS_NAME}' >> /vps/${WEBVPS_NAME}/.env
echo 'export WEBVPS_WORKER_UID=$(id -u ${WEBVPS_NAME})' >> /vps/${WEBVPS_NAME}/.env
echo 'export WEBVPS_HOST=lescyclomondistes.com' >> /vps/${WEBVPS_NAME}/.env
echo 'export WEBVPS_DOMAINS=${WEBVPS_HOST},www.${WEBVPS_HOST}' >> /vps/${WEBVPS_NAME}/.env
echo "export WEBVPS_DB_PASSWORD=${WEBVPS_DB_PASSWORD}" >> /vps/${WEBVPS_NAME}/.env
echo "export WEBVPS_DB_ROOT_PASSWORD=${WEBVPS_DB_ROOT_PASSWORD}" >> /vps/${WEBVPS_NAME}/.env
```

**Ajouter un accès FTP aux volumes**

```
docker exec -it ftpd_server bash -c "pure-pw useradd ${WEBVPS_NAME} -f /etc/pure-ftpd/passwd/pureftpd.passwd -m -u $(id -u ${WEBVPS_NAME}) -g $(id -u ${WEBVPS_NAME}) -d /home/ftpusers/${WEBVPS_NAME}"
```
et saisir le même mot de passe que celui utilisé pour créer le compte sur le serveur hôte.

**Créer le fichier docker-compose.yml** pour décrire le service en utilisant les variables d'environnement se trouvant dans ```.env```

**Paramétrer les DNS**

Créez une entrée de type A vers l'IP du serveur (idéalement l'IP FailOver) et une entrée du type CNAME vers cette entrée de type A. Exemple :

```
example.com. A w.x.y.z
www.example.com. CNAME example.com
```

**Monter le service**

```
cd /vps/${WEBVPS_NAME}
vps-compose up
```

**Ajouter ce service à votre service de monitoring**