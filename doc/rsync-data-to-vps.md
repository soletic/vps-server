# Rsync data to VPS

If you want upload file from another hosting having an ssh access, you can use rsync

To use an authentification by ssh key, add the public key of the user `ubuntu`
to the keys authorized by the source user.

```bash
export WEBVPS_NAME=vps_example
mkdir -p /mnt/data/vps/${WEBVPS_NAME}/html
sudo chown -R `id -u`:`id -u` /mnt/data/vps/${WEBVPS_NAME}
rsync -az login@serveur.org:source/ /mnt/data/vps/${WEBVPS_NAME}/html
# example
# rsync -az soleticobg@ssh.cluster028.hosting.ovh.net:wordpress/tourismesport/ /mnt/data/vps/${WEBVPS_NAME}/html
sudo chown -R ${WEBVPS_NAME}:${WEBVPS_NAME} /mnt/data/vps/${WEBVPS_NAME}
```
