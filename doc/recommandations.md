# Recommandations

## Se baser au maximum sur les images docker officielles

Les images docker officielles ont l'avantage d'être régulièrement maintenues
par une large communauté. Elles sont aussi bien documentées sur la façon de
les installer et les mettre à jour.

## Créer ses propres images de micro-service

Chaque VPS propose des applications en lançant les images de différents micro-services.

Par exemple pour Nextcloud, nous avons au moins 2 micro-services :

- Nextcloud lui-même
- MariaDB pour la base de données

En créant le fichier docker-compose.yml, vous pouvez directement utiliser l'image
docker officielle ou passer par l'intermédiaire de votre propre image héritant de l'image officielle.

La recommandation est de **passer par l'intermédiaire de votre propre image héritant de l'image officielle.**.
Cette pratique comporte deux avantages principaux :

- Personnalisation possible de l'image par rapport aux propres besoins de votre déploiement
- Si un autre VPS tire l'image officielle sur le serveur, les autres VPS, même en se reconstruisant, resterons sur la version de l'image utilisée lors de leur dernière construction.
