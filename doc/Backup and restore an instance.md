# Backup and restore an instance

To restore an instance you need to backup it. From OVH Public cloud interface,
activate the automatic backup.

If you need to restore, follow the documentation : https://docs.ovh.com/fr/public-cloud/creer-restaurer-un-serveur-virtuel-a-partir-dune-sauvegarde/
