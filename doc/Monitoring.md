# Monitoring

## Disponibilité des services HTTP

Nous utilons le service UptimeRobot pour vérifier toutes les 5 minutes la disponibilité d'un service. C'est gratuit jusqu'à 50 services surveillés. De plus, avec un compte Pushbullet connecté comme contact, les alertes peuvent être paramétrés pour être reçus sur téléphone et navigateur internet.

## Disk space

Si des problèmes de place disque surviennent, il faut déterminer ce qui prend de la place et le nettoyer.

### Repérer ce qui prendre la place

```bash
sudo du -sch --exclude /mnt --exclude /proc --exclude /var/lib/docker/overlay2 /*
docker system df
```

### Clean disk space used by docker

- Lire [Clean docker data and save disk space](Clean%20docker%20data%20and%20save%20disk%20space.md)
