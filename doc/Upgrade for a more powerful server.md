# Upgrade for a more powerful server

## Sur cloud OVH

Pré-requis : avoir utilisé l'IP FailOver et que chaque domaine hébergé par le serveur pointé vers cet IP FailOver

**Créer un snapshot du server**

Et lancer un nouveau serveur à partir du snapshot.

Si c'est pour encaisser une charge plus importante sur une courte durée, choisissez la facturation à l'heure ET conservez bien le serveur précédent !

**Stopper le serveur**

```
sudo service docker stop
umount /mnt/data
umount /mnt/backup
```

**Déplacer les disques**

Pour chaque disque, en commençant par le premier de la liste, modifier le serveur associé.

**Associer l'IP FailOver au nouveau serveur

