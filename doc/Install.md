# Install

## Rappels

- **Service** : un service est un ensemble de containers, orchestré via docker-compose, et offrant un service (site internet, tâche cron, ...)

## Comprendre les bases

L'architecture est conçue pour fonctionner en mode mono serveur (pas de cluster) car cela couvre l'essentiel de la plupart des utilisateurs. Si vous cherchez une solution pour des services soumis à des fortes variations de charge ou peu anticipable, alors ce que nous proposons ici n'est pas adapté.

Nous utilisons l'offre cloud public d'OVH pour les avantages suivants :

- Snapshot du serveur permettant une restauration du ```runtime``` des services très rapidement (containers docker)
- Disque dur cloud avec garantie 100% zéro-perte des données (triple réplication) et stockant les données des services (volumes des containers)
- Migration rapide vers un serveur plus puissant si besoin par restauration du snapshot et déplacement du disque dur sur nouvelle instance.

## Server

Une fois le projet cloud public créé dans votre espace OVH, vous creéez une instance serveur sur une base Ubuntu 18.04 Server puis :

```
sudo apt-get update
sudo apt-get -y install docker \
    docker-compose \
    pwgen \
    jq
sudo docker network create proxy
sudo mkdir /vps
sudo gpasswd -a $USER docker
sudo chown -R ubuntu:ubuntu /vps
```

Puis installer les sources et binaires de l'outil de gestion du serveur dans ```/usr/local/vps-server```

```
cd /usr/local
sudo git clone https://framagit.org/soletic/vps-server.git
sudo mkdir -p /vps
sudo chown -R ubuntu:ubuntu /usr/local/vps-server
sudo chown -R ubuntu:ubuntu /vps
echo 'PATH="/usr/local/vps-server/bin:$PATH"' >> ~/.profile
server-docker-build-libimages
```

## Disque dur cloud OVH à attacher sur une instance

Attacher un disque dur au serveur puis en [s'inspirant du guide suivant](https://docs.ovh.com/fr/public-cloud/creer-et-configurer-un-disque-supplementaire-sur-une-instance/), les manipulations suivantes ont été faites.

Déterminer le chemin de la partition : ```lsblk```. On trouve ```/dev/sdb```

Puis exécuter ```sudo fdisk /dev/sdb```. Exécutez la commandes ```n``` et conservez les valeurs par défaut proposés pour la création. Dernière commande : ```w``` pour écrire la partition.

Puis :

```
sudo mkfs.ext4 /dev/sdb1
sudo mkdir /mnt/data
sudo mount /dev/sdb1 /mnt/data/
sudo blkid
    + UUID="b394ffb2-3e77-4eac-86de-66ef41148b8e" TYPE="ext4" PARTUUID="79c5fe09-01"
sudo vim /etc/fstab
    + UUID=b394ffb2-3e77-4eac-86de-66ef41148b8e /mnt/data ext4 nofail 0 0
```

## Quota disk space management

L'objectif est de limiter l'espace disque utilisé par chaque service. Cette solution a été inspirée par

- [cette discussion sur les issues de Docker](https://github.com/docker/docker/issues/471#issuecomment-22715725)
- [ce post](https://www.digitalocean.com/community/questions/enable-quota-ubuntu-16-04-error?answer=36983) et [ce post](https://askubuntu.com/questions/109585/quota-format-not-supported-in-kernel) qui font comprendre pourquoi sur un kernel virtuel il faut installer certains paquets qui par défaut sont là sur un serveur "en dur"

```
sudo apt-get update
sudo apt-get -y install quota quotatool
sudo apt-get -y install linux-image-generic
sudo apt-get -y install linux-headers-generic
sudo apt-get -y install linux-image-extra-`uname -r`
```

Il vous sera demandé de faire un choix pour grub. Surtout, conservez la verion déjà installée localement.

```
sudo -i
echo quota_v1 >> /etc/modules
echo quota_v2 >> /etc/modules
modprobe quota_v2
modprobe quota_v1
exit
```

Editer le fichier /etc/fstab pour ajouter l'option ```usrjquota=aquota.user,grpjquota=aquota.group,jqfmt=vfsv0``` au montage du disque dur cloud. Exemple :

```
UUID=quelque-chose-uuid /mnt/data ext4 nofail,usrjquota=aquota.user,grpjquota=aquota.group,jqfmt=vfsv0 0 0
```
Démonter et remonter le disque :

```
sudo mount -o remount /mnt/data
```
Àctiver les quotas

```
sudo quotacheck -cguvmf /mnt/data
sudo quotaon -avug
```

## Stack Docker

**Configure log driver to limit log size**

Source : https://docs.docker.com/config/containers/logging/configure/

Example of the `/etc/docker/deamon.json` file :

```json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "max-file": "3"
  }
}
```

**Setup proxy**

```
sudo mkdir /mnt/data/vps
sudo chown -R ubuntu:ubuntu /mnt/data/vps
mkdir -p /mnt/data/vps/proxy/nginx/conf
curl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl > /mnt/data/vps/proxy/nginx/nginx.tmpl
echo 'client_max_body_size 512m;' > /mnt/data/vps/proxy/nginx/conf/my_proxy.conf
docker run --net proxy -d -p 80:80 -p 443:443 \
    --name http-proxy \
    --restart unless-stopped \
    -v /mnt/data/vps/proxy/certs:/etc/nginx/certs:ro \
    -v /mnt/data/vps/proxy/nginx/vhost.d:/etc/nginx/vhost.d \
    -v /mnt/data/vps/proxy/nginx/html:/usr/share/nginx/html \
    -v /mnt/data/vps/proxy/nginx/conf/my_proxy.conf:/etc/nginx/conf.d/my_proxy.conf:ro \
    -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
docker run --net proxy -d \
    --name ssl-generator \
    --restart unless-stopped \
    -v /mnt/data/vps/proxy/certs:/etc/nginx/certs:rw \
    --volumes-from http-proxy \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    jrcs/letsencrypt-nginx-proxy-companion
```

**Setup ftp access**

L'accès FTP aux volumes d'un VPS se fait via un container dédié à cela grâce à l'image docker [stilliard/docker-pure-ftpd](https://github.com/stilliard/docker-pure-ftpd)

```
sudo mkdir /mnt/data/vps/ftpd
sudo mkdir -p /mnt/data/vps/ftpd/users
sudo mkdir -p /mnt/data/vps/ftpd/passwd
docker run -d --name ftpd_server -p 21:21 -p 30000-30009:30000-30009 \
  --restart unless-stopped \
  -e "PUBLICHOST=ftp.vps-01.cloud.soletic.org" \
  -e "FTP_MAX_CLIENTS=50" \
  -v /mnt/data/vps:/home/ftpusers \
  -v /mnt/data/vps/ftpd/passwd:/etc/pure-ftpd/passwd \
  soletic/ftpd:1.0

```

**Enable docker log rotate**

```bash
sudo vi /etc/logrotate.d/docker-container
```

And tape in :

```
/var/lib/docker/containers/*/*.log {
  rotate 7
  daily
  compress
  size=1M
  missingok
  delaycompress
  copytruncate
}
```

Once you have configured Logrotate for you Docker container you can test it with ```sudo logrotate -fv /etc/logrotate.d/docker-container```

## Backup

### Add cronjob for mysql backup

Tous les containers faisant touner une base de données mysql/mariadb se terminent par ```.mysql``` ou `.mariadb`. Le script [automysqlbackup](./bin/automysqlbackup) s'installe dans ```/vps``` et un lien symbolique dans ```/etc/cron.daily``` permet de le lancer chaque jour pour sauvegarder les base (via le  script ```automysqlbackup``` présent dans chacun des containers)

```
chmod u+x /vps/automysqlbackup
sudo ln -s  /usr/local/vps-server/bin/automysqlbackup /etc/cron.daily/automysqlbackup
```

### Add cronjob for wordpress container

[Read deploy strategy for wordpress](apps/wordpress.md)

```
chmod u+x /usr/local/vps-server/bin/wp-cron
sudo ln -s  /usr/local/vps-server/bin/wp-cron /etc/cron.hourly/wp-cron
```

### Backup vps sources

Créer une tâche cron effectuant un rsync de /vps vers /mnt/data/backup/vps

```
sudo vi /etc/cron.hourly/vps-backup
sudo chmod u+x /etc/cron.hourly/vps-backup
sudo mkdir -p /mnt/data/backup/vps
```

```
#!/bin/bash

rsync --delete -av /vps/ /mnt/data/backup/vps
```

### Replication data (optionnel)

Si vous craignez des personnes de données et le non respect d'OVH d'une garantie de durabilité des données à 100% sur les disques additionnels, vous pouvez monter un deuxième disque dur. dans notre cas /mnt/backup et créer une tâche cron effectuant un rsync de /mnt/data vers /mnt/backup

```
sudo vi /etc/cron.daily/vps-replication
sudo chmod u+x /etc/cron.daily/vps-replication
```

```
#!/bin/bash

rsync --delete -av /mnt/data/ /mnt/backup
```

### IP FailOver

Utilisez une adresse IP flottante (2€/mois), attachez à votre instance serveur de façon à paramétrer les DNS des services vers cette IP. Ainsi, si vous changez d'instance, vous déplacez cette IP vers la nouvelle instance et aucune modification n'est à faire sur les DNS de l'ensemble des services.

- [Documentation OVH](https://docs.ovh.com/fr/public-cloud/rendre-la-configuration-ip-fail-over-persistante/#configuration-de-lip-fail-over_1)
- [Post sur forum OVH pour config Ubuntu 17+](http://community.ovh.com/t/configuration-ip-failover-avec-netplan-ubuntu-17-10/6157/2)

Éditer le fichier de configuration avec la commande  ```sudo vi /etc/netplan/51-failover.yaml```

Ajouter en fin de fichier :

```
network:
    version: 2
    vlans:
        veth0:
            id: 0
            link: ens3
            dhcp4: no
            addresses: [188.165.40.231/24]
```

Puis appliquer : ```sudo netplan apply```

## Optionnal

### Setup swarn

Useful for docker secret.

```
docker swarm init --advertise-addr=188.165.40.231
```

We got :

```
Swarm initialized: current node (jywtf1iu8bwjrtzrghqw3tu28) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3912zr0z34iszuwjdjatwbl8uwx0e3lwpq5ft419kjss216a25-9ahi9ydfix07j5wzwf9eyeay1 188.165.40.231:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

### Se connecter à un compte docker pour les dépôts privés

La commande ```docker login``` échoue avec l'erreur : ```Error saving credentials: error storing credentials - err: exit status 1, out: Cannot autolaunch D-Bus without X11 $DISPLAY```

Nous avons suivi ce commentaire pour contourner le problème : https://github.com/docker/compose/issues/6023#issuecomment-425197890
