# Limits

It 's possible the have following message :

```could not find an available, non-overlapping IPv4 address pool among the defaults to assign to the network```

You should remove unused default network with :

```docker network prune```