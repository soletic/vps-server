# MariaDB / MySQL

## Usage au sein de la solution

Lorsque vous créez des applications dans votre VPS, nous conseillons toujours de créer une image docker
propre à chaque micro-service utilisé. Par exemple, pour le micro-service mariadb,
vous créez le sous-dossier images/mariadb/Dockerfile dans le dossier de votre VPS (lire [doc/recommandations.md](doc/recommandations.md))

```
/path/to/your-vps-app
--|
  | images
  --| mariadb
    --| Dockerfile
```

et contenant

```
# Dockerfile
FROM soletic/mariadb:10.4
MAINTAINER You <you@mail.com>
```

Dans le cas de MariaDB, la solution *VPS Server* propose ses propres images MariaDB
héritant des images officielles pour y intégrer deux fonctionnalités essentielles :

- Un script de sauvegarde des bases de données
- L'utilisation d'un identifiant d'utilisateur différent de celui proposé par défaut par l'image officielle.

## Mise niveau du micro-service mariadb d'un VPS

Editer le fichier `Dockerfile` du micro-service MariaDB de votre VPS et modifier la version
dont il hérite. Puis exécuter les commandes suivantes :

```bash
cd /path/to/vps
. .env
# Backup bdd et micro-service
docker exec ${WEBVPS_NAME}.mysql automysqlbackup
vps-compose stop
docker tag soleticnc_db ${WEBVPS_NAME}/mysql:backup
# Recreate
vps-compose build
vps-compose recreate
```

> Généralement il faut tout reconstruire car tous les micro-services se basent sur celui de mariadb.

Suite le lancement du container de base de données

```bash
docker logs --tail 100 -f ${WEBVPS_NAME}.mysql
```

## Construction des différentes versions des images mariadb

La plupart des applications utilisent MariaDB. De temps en temps il faut les mettre à jour
pour qu'ils utilisent une version plus récente de MariaDB.

### Si l'image existe déjà dans le code source de VPS Server

Les différentes versions se trouvent dans le dépôt [library/images/mariadb](library/images/mariadb)

Actualisez le dépôt :

```bash
cd /usr/local/vps-server
git pull
```

Puis construire l'image (exemple ici de MariaDB 10.4):

```bash
cd /usr/local/vps-server/library/images/mariadb
docker build -t soletic/mariadb:10.4 -f 10.4/Dockerfile .
```

Voici les instructions de mises à jour d'une version majeure à une autre.

### Créer une nouvelle version majeure

Si la version majeure n'existe pas encore dans le dépôt [library/images/mariadb](library/images/mariadb)
alors il faut la créer en produisant le fichier Dockerfile tout en s'inspirant de la version précédente.

1. Créer le fichier `Dockerfile` dans `/usr/local/vps-server/library/images/mariadb/x.y`
2. Dans `/usr/local/vps-server/bin/server-docker-build-libimages` ajouter la construction de l'image
3. Construire votre image

```bash
cd /usr/local/vps-server/library/images/mariadb
docker build -t soletic/mariadb:x.y -f x.y/Dockerfile .
```

Pour faire profiter de votre travail aux autres, effectuez un merge request sur le dépôt.

## Instructions de mises à jour

Aucune instructions spécifiques pour l'instant.
