# Personnal server of web services with docker

**Pile d'outils pour aider à installer, personnaliser et héberger en toute sécurité des services Internet hétérogènes, via docker et docker-compose sur un seul serveur** :

- Qualité de service : disponibilité des services et sauvegarde des données
- Mutualisation sur un seul serveur pour économiser
- Adaptable en moins de 10 minutes pour répondre à une augmentation de charge, passagère ou continue.

## Pour qui ?

Il s'adresse à des techniciens

- Connaissant un peu l'administration de serveur et docker
- Très bidouilleurs, plutôt DIY
- A la recherche d'une solution souple et d'un groupe d'entraide pour adopter un état d'esprit **laboratoire**

Il doit rester un cadre simple et **inspirant pour d'autres**. Ainsi il n'a pas vocation à être transformeé en une usine à gaz comblant tout le monde mais à être un socle commun de plusieurs collectifs souhaitant le dupliquer, le personnaliser et re-partager des bonnes pratiques auprès de tous.

## Architecture et technologies

L'architecture est conçue pour fonctionner en mode mono serveur (pas de cluster) car cela couvre l'essentiel des besoins de la plupart des utilisateurs. Si vous cherchez une solution pour des services soumis à des fortes variations de charge et peu anticipables, alors ce que nous proposons ici n'est pas adapté.

**Technologies**

- Ubuntu 18.04
- Docker et docker-compose
- Offre cloud public d'OVH
- Paquets quota et quotatool pour la gestion de l'espace disque des services.

## Install and typical use case

- [Install](doc/Install.md)
- [Monitoring](doc/Monitoring.md)

Après l'installation, voici un exemple de ce que vous pourrez faire en toute simplicité :

```TODO```

## Todo

### Upgrade to Ubuntu 22.04

L'upgrade va permettre d'utiliser les dernières versions de docker et docker-compose et notamment la version 3.7 qui permet ce type de configuration : https://github.com/knadh/listmonk/blob/master/docker-compose.yml

- Création d'extensions pour réutiliser les configurations de services
- Option `healthcheck` pour les services

Les points de vigilance :

- Est-ce que les docker-compose.yml en version 2 vont rester compabtibles ?
- Comment mettre en place les limites de ressources (cpu, mémoire) pour les services ? Début de réponse avec la section "deploy" : https://docs.docker.com/compose/compose-file/compose-file-v3/#deploy

### Ideas

- FTP
	- Add SSL/TLS for ftp
- Comment faire pour installer snapshot sur serveur avec moins de HDD sur OVH ?
	- Il faut créer un serveur avec HDD flexible (mini 50G). Pb : sandbox fait 40G.
- vps-add-user à terminer
- Surveillance de charge (alerte si trop chargé trop longtemps)
- Surveillance d'espace disque
- server-init
	- Check dependency
- Ecrire la section ```Install and typical use case``` de ce fichier Readme pour montrer la simplicité d'utilisation
- Finir paramétrage du système de quota
- Fournir des modèles de docker-compose
- Synchroniser sauvegarde de fichiers et sauvegarde de base avec possivilité de faire une sauvegarde d'apps et un restore si jamais une mise à jour échoue.
