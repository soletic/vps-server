
MYSQL_USER="mysql"
MYSQL_GROUP="mysql"
if [ ! -f /etc/${WORKER_UID} ]; then
	OLDUID=$(id -u ${MYSQL_USER})
	usermod -s /bin/bash ${MYSQL_USER}
	usermod -u ${WORKER_UID} ${MYSQL_USER}   
	groupmod -g ${WORKER_UID} ${MYSQL_GROUP}
	find / -user ${OLDUID} -exec chown -h ${WORKER_UID} {} \;
	find / -group ${OLDUID} -exec chgrp -h ${WORKER_UID} {} \;
	usermod -g ${WORKER_UID} ${MYSQL_USER}
	touch /etc/${WORKER_UID}
fi
