# php7-fpm/Dockerfile
FROM php:7.2-apache
MAINTAINER Laurent CHEDANNE <laurent@chedanne.pro>

RUN apt-get update && apt-get install -y \
    git \
    unzip \
    expect

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# Set timezone
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"

# --- Rewrite engine
RUN a2enmod rewrite

# Enable more memory for php execution
RUN echo "memory_limit=512M" >> /usr/local/etc/php/php.ini

# -- Zip extension for phpunit
RUN apt-get -y install libzip-dev && docker-php-ext-install zip
# Add mysql
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli
# Intl extension
RUN apt-get -y install libicu-dev && \
    docker-php-ext-install intl
# Gd extension
RUN apt-get -y install libpng-dev libfreetype6-dev libjpeg62-turbo-dev && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install gd

# Run as specific uid
ENV WORKER_UID 1000
COPY bin/apache-run /usr/local/bin/
CMD ["apache-run"]
