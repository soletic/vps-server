
if [ ! -f /etc/${WORKER_UID} ]; then
	if [ ! -z "${WORKER_UID}" ]; then
		OLDUID=$(id -u www-data)
		usermod -s /bin/bash www-data
		usermod -u ${WORKER_UID} www-data   
		groupmod -g ${WORKER_UID} www-data
		find / -user ${OLDUID} -exec chown -h ${WORKER_UID} {} \;
		find / -group ${OLDUID} -exec chgrp -h ${WORKER_UID} {} \;
		usermod -g ${WORKER_UID} www-data
		touch /etc/${WORKER_UID}
	fi
fi
