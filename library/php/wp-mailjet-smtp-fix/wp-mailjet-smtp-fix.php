<?php
/*
Plugin Name: WP Mailjet SMTP Spam Fix
Description: Force l'entête HTML pour les mails envoyés afin de contourner les décisions de Mailjet de pré-bloquer certains emails
Version: 0.1
Author: Laurent Chedanne @lchedanne
Author URI: http://chedanne.pro
Text Domain: wp-mailjet-smtp-fix
*/

// Customize lost password email because considered as spam by Mailjet
add_filter( 'retrieve_password_message', 'soletic_replace_retrieve_password_message', 10, 4 );
add_filter( 'retrieve_password_title', 'soletic_replace_retrieve_password_title', 10, 3 );
add_filter( 'wp_mail_content_type', 'soletic_mail_set_html_content_type' );
function soletic_replace_retrieve_password_title( $message, $key, $user_login, $user_data ) {
    return sprintf(__( 'Réinitialiser votre mot de passe sur %s', 'wp-mailjet-smtp-fix' ), get_site_url());
}
function soletic_mail_set_html_content_type()
{
    return 'text/html';
}
/**
 * Returns the message body for the password reset mail.
 * Called through the retrieve_password_message filter.
 *
 * @param string  $message    Default mail message.
 * @param string  $key        The activation key.
 * @param string  $user_login The username for the user.
 * @param WP_User $user_data  WP_User object.
 *
 * @return string   The mail message to send.
 */
function soletic_replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
    $url = site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' );
    // Create new message
    $msg  = __( '<p>Bonjour!</p>', 'wp-mailjet-smtp-fix' );
    $msg .= sprintf( __( '<p>Vous avez demandé à redéfinir votre mot de passe pour le compte <strong>%s</strong>.</p>', 'wp-mailjet-smtp-fix' ), $user_login );
    $msg .= __( "<p>Si c'est une erreur, ignorez simplement ce message.</p>", 'wp-mailjet-smtp-fix' );
    $msg .= sprintf(__( '<p><a href="%s">Cliquez-ici pour redéfinir le mot de passe</a> ou copier-coller l\'adresse suivante dans votre navitageur internet : %s</p>', 'wp-mailjet-smtp-fix' ), $url, $url);
    $msg .= __( '<p>Merci!</p>', 'wp-mailjet-smtp-fix' );
    return $msg;
}